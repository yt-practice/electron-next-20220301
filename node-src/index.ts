/* eslint-disable no-console */
import { app, BrowserWindow, protocol } from 'electron'
import fastify from 'fastify'
import next from 'next'

const isDev = process.env.NODE_ENV !== 'production'
const nextApp = next({
	dev: isDev,
	minimalMode: true,
	customServer: true,
	hostname: 'next.app',
})
const handle = nextApp.getRequestHandler()

type ValidMethod =
	| 'DELETE'
	| 'GET'
	| 'HEAD'
	| 'OPTIONS'
	| 'PATCH'
	| 'POST'
	| 'PUT'
const isValid = (m: string): m is ValidMethod =>
	'DELETE' === m ||
	'GET' === m ||
	'HEAD' === m ||
	'OPTIONS' === m ||
	'PATCH' === m ||
	'POST' === m ||
	'PUT' === m

app.on('ready', () => {
	const fApp = fastify({ logger: { level: 'error' }, pluginTimeout: 0 })
	console.log({ isDev })
	fApp.addHook('onRequest', (req, res, done) => {
		// @ts-expect-error: ignore
		req.raw.originalRequest ||= req.raw
		done()
	})
	if (isDev) {
		fApp.get('/_next/*', async (req, reply) => {
			await handle(req.raw, reply.raw).then(() => {
				reply.sent = true
			})
		})
	}
	fApp.all('/*', async (req, reply) => {
		await handle(req.raw, reply.raw)
			.then(() => {
				console.log('found')
				reply.sent = true
			})
			.catch(x => {
				console.log('not found')
				console.error(x)
				throw x
			})
	})
	// eslint-disable-next-line @typescript-eslint/no-misused-promises
	fApp.setNotFoundHandler(async (req, reply) => {
		await nextApp.render404(req.raw, reply.raw).then(() => {
			reply.sent = true
		})
	})
	protocol.registerBufferProtocol('next', (req, done) => {
		if (!isValid(req.method)) throw new Error('unknown method.')
		const buf =
			req.uploadData && Buffer.concat(req.uploadData.map(d => d.bytes))
		const url = new URL(req.url)
		fApp.inject(
			{
				method: req.method,
				url: url.pathname,
				query: Object.fromEntries(url.searchParams.entries()),
				payload: buf,
				headers: req.headers,
			},
			(err, res) => {
				// console.log({
				// 	req,
				// 	err,
				// 	res: {
				// 		statusCode: res.statusCode,
				// 		headers: res.headers,
				// 		payload: res.payload,
				// 	},
				// })
				if (err) {
					console.error(err)
					done({ error: 500, data: '' })
					return
				}
				done({
					statusCode: res.statusCode,
					// @ts-expect-error: ignore
					headers: res.headers,
					data: res.rawPayload,
				})
			},
		)
	})
	nextApp
		.prepare()
		.then(async () => {
			const mainWindow = new BrowserWindow({
				title: 'test',
				width: 800,
				height: 600,
				minWidth: 800,
				minHeight: 600,
				webPreferences: {
					nodeIntegration: false,
					contextIsolation: true,
					// preload: join(__dirname, 'preload.js'),
				},
			})

			await mainWindow.loadURL('next://next.app/')
		})
		.catch(x => {
			console.error(x)
		})
})

// Quit the app once all windows are closed
app.on('window-all-closed', () => {
	app.quit()
})
