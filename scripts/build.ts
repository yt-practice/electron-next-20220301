import { build } from 'esbuild'
import { nodeExternalsPlugin } from 'esbuild-node-externals'

build({
	entryPoints: ['node-src/index.ts'],
	bundle: true,
	outdir: 'node-dist',
	plugins: [nodeExternalsPlugin()],
	minify: true,
	platform: 'node',
	target: 'node14',
	define: { 'process.env.NODE_ENV': JSON.stringify('production') },
}).catch(x => {
	// eslint-disable-next-line no-console
	console.error(x)
	process.exit(1)
})
