import Link from 'next/link'

const Home = () => (
	<div>
		<h1>home</h1>
		<ul>
			<li>
				<Link href="/fuga">
					<a>fuga</a>
				</Link>
			</li>
			<li>
				<Link href="/piyo">
					<a>piyo</a>
				</Link>
			</li>
		</ul>
	</div>
)

export default Home
