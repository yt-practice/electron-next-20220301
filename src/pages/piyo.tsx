import Link from 'next/link'

const Piyo = () => (
	<div>
		<h1>piyo</h1>
		<ul>
			<li>
				<Link href="/">
					<a>home</a>
				</Link>
			</li>
			<li>
				<Link href="/fuga">
					<a>fuga</a>
				</Link>
			</li>
		</ul>
	</div>
)

export default Piyo
