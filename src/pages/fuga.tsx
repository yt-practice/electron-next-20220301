import Link from 'next/link'

const Fuga = () => (
	<div>
		<h1>fuga</h1>
		<ul>
			<li>
				<Link href="/">
					<a>home</a>
				</Link>
			</li>
			<li>
				<Link href="/piyo">
					<a>piyo</a>
				</Link>
			</li>
		</ul>
	</div>
)

export default Fuga
